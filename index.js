console.log('Hello World');

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruits'];

console.log("Current array: ");
console.log(fruits);

// array push method

let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log("Mutated array push method");
console.log(fruits);

let addFruits =  fruits.push('avocado',);

 console.log(fruits);

// array pop method

// let removedFruit = fruits.pop();
// console.log(removedFruit);
// console.log("Mutated array from pop method: ");
// console.log(fruits);


//Unshift() Array method

fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method: ");
console.log(fruits);

//Shift() Array method

fruits.shift('Lime');
console.log("Mutated array from shift method: ");
console.log(fruits);

//Splice() Array method
fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method: ");
console.log(fruits);

//Sort() Array method
fruits.sort();
console.log("Mutated array from sort method: ");
console.log(fruits);

//Reverse() Array method
fruits.reverse();
console.log("Mutated array from reverse method: ");
console.log(fruits);

//Non-Mutator Methods
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

//indexOf()
//Syntax --> arrayName.indexOf(searchValue);
//Syntax --> arrayName.indexOf(searchValue, fromIndex);

let firstIndex = countries.indexOf("PH");
console.log(`Result of indexOf method: ${firstIndex}`);

let invalidCountry = countries.indexOf("BR");
console.log(`Result of indexOf method: ${invalidCountry}`);

//lastIndexOf()
//Syntax --> arrayName.lastIndexOf(searchValue);
//Syntax --> arrayName.lastIndexOf(searchValue, fromIndex);

//getting the index number starting from the last element
let lastIndex = countries.lastIndexOf("PH");
console.log(` Result of lastIndexOf method  ${lastIndex}`);


//getting the index number starting from a specified  index
let lastIndexStart = countries.lastIndexOf("PH", 5  );
console.log(` Result of lastIndexOf method  ${lastIndexStart}`);


//Slice() array method
let sliceArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(sliceArrayA);

//Slice() array method
let sliceArrayB = countries.slice(2,4);
console.log("Result from slice method:");
console.log(sliceArrayB);


//Slice() array method
let sliceArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(sliceArrayC);


//toString() array method
let stringArray = countries.toString();
console.log("Result from toSting method:");
console.log(screen);


//concat()
let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe sass"];
let taskArrayC = ["get git", "be node"];

let task = taskArrayA.concat(taskArrayB);
console.log("Result from concat method:");
console.log(task);

//Combining multiple arrys
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

//Combining arrays with elements
let combinedTasks = taskArrayA.concat("smeel express", "throw react");
console.log("Result from concat method:");
console.log(combinedTasks);

//join() Array methods
let users = ["Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' ! '));

//Iteration Methods
//Iteration are loop[s design to perform repetitive task on arrays

//forEach()

allTasks.forEach(function(task) {
  console.log(task);
})


//Using forEach with conditional statement

let filteredTasks = [];

// Looping through all array Items

allTasks.forEach(function(task) {
     if(task.length > 10){
        filteredTasks.push(task);
     }
});

console.log("Result of filtered tasks: ");
console.log(filteredTasks);


//map()
//Syntax let/const resultArray = arrayName.map(function(indivElement))

let numbers = [1, 2, 3, 4, 5];

//required return statement
let numberMap = numbers.map(function(number) {
    return number * number;
});

console.log(`Original Array: `);
console.log(numbers); //original is unaffected by map()
console.log(`Result of map method: `)
console.log(numberMap);


//map() vs forEach()

let numberForEach = numbers.forEach(function(number){
      return number * number;
})

console.log(numberForEach); //undefined result


//every()
/* 
  Syntax --> let/const resultArray = arrayName.every(function(){
      return expression/condition
  })

*/

let allValid = numbers.every(function(number) {
  return ( number > 2 )
})


console.log("Result of every method: ");
console.log(allValid);


//some() array method
let someValid = numbers.some(function(number) {
  return (number < 2);
})

console.log("Result from some method:");
console.log(someValid);

// if the condiotion even only element the result is true
if(someValid){
  console.log('Some numbers in the array are greater than 2');
}

//finter() array method
/*

  Syntax --> let/const resultArray = arrayName.filter(function(indivElement){
            return expression/condition
  })

*/

let filterValid = numbers.filter(function(number){
  return (number < 4);
});

console.log(`Result from filter method:`);
console.log(filterValid);


let nothingFound = numbers.filter(function(number){
  return (number = 0);
});

console.log(`Result from filter method:`);
console.log(nothingFound); //it will return empty bracket

//filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number) {
    if(number < 3){
      filteredNumbers.push(number)
    }
});


console.log(`Result from forEach method:`);
console.log(filteredNumbers);


//include() array method function;

/*

  Syntax --> arrayName.include(<argument>)

*/
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let productFound1 = products.includes("Mouse");


console.log(`Result from forEach method:`);
console.log(productFound1);

let productFound2 = products.includes("camera");
console.log(productFound2);

//reduce() array method function
/* 
      Syntax --> let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
            return expression/condition
    })
*/

let iteration = 0;

let reduceArray = numbers.reduce(function(x, y) {
      console.warn('current iteration' + ++iteration);
      console.log('accumulator: ' + x);
      console.log('currentValue: ' + y);

      return x + y;
});


console.log(`Result from reduce method: ` + reduceArray);



console.log(`Result from forEach method:`);
console.log(filteredNumbers);
